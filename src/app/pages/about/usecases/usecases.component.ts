import { Component, OnInit } from '@angular/core';
import { UseCase } from '../usecase.model';

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.css'],
})
export class UsecasesComponent implements OnInit {
  readonly PLAIN_USER = 'Reguliere gebruiker';
  readonly ADMIN_USER = 'Administrator';
  readonly CHEF_USER = 'Chefkok'

  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.',
      ],
      actor: this.PLAIN_USER,
      precondition: 'Geen',
      postcondition: 'De actor is ingelogd',
    },
    {
      id: 'UC-02',
      name: 'adding food type',
      description: 'Hiermee kan een kok een food type toevoegen ',
      scenario: ['de kok logt in', 'De kok navigeert naar type of meals tab', 'De kok drukt op toevoegen nieuw type of meal, de kok voert alle informatie correct in', 'De kok drukt op toevoegen'],
      actor: this.CHEF_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'De type of meal is toegevoegd aan de database.',
    },

    {
      id: 'UC-03',
      name: 'updating food type',
      description: 'Hiermee kan een kok een food type updaten ',
      scenario: ['de kok logt in', 'De kok navigeert naar type of meals tab', 'De kok drukt op een type of meal', 'de kok drukt op edit','De kok update de benodigde informatie' ,'De kok drukt op updaten'],
      actor: this.CHEF_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'De type of meal is geupdate in de database.',
    },
    
    {
      id: 'UC-04',
      name: 'deleting food type',
      description: 'Hiermee kan een admin een food type deleten ',
      scenario: ['de admin logt in', 'De admin navigeert naar type of meals tab', 'De kok drukt op een type of meal', 'de kok drukt op delete'],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'De type of meal is gedelete uit de database.',
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}
