import { Injectable } from '@angular/core';
import { from, Observable, of } from 'rxjs';
import { delay, filter, mergeMap, take } from 'rxjs/operators';
import { Foodtype } from './foodtype.model'
@Injectable({
  providedIn: 'root',
})
export class FoodtypeService {
  private foodtypes: Foodtype[] = [
    {
    id: 1,
    name: 'Stampot',
    alternate_name: 'Hutspot',
    country_of_origin: 'The Netherlands',
    allergies: 'Non',
    trivia: 'A Farmers meal that is quick and easy to make',
    },
    {
    id: 2,
    name: 'Spaghetti',
    alternate_name: 'European noodles',
    country_of_origin: 'Italy',
    allergies: 'Gluten',
    trivia: 'Stolen from China',
    },
  ];

  constructor() {}

  getAll(): Observable<Foodtype[]> {
    // 'of' maakt een Observable<User[]>
    return of(this.foodtypes).pipe(delay(1500));
  }

  getById(id: number): Observable<Foodtype> {
    // 'from' maakt een array van Observable<User>
    // Van dat array willen we 1 waarde, met de gevraagde id.
    return from(this.foodtypes).pipe(
      filter((item) => item.id === id),
      take(1)
    );
  }

delete(id: number): Observable<Foodtype> {
    for(var i = 0; i<this.foodtypes.length; i++){
      if(this.foodtypes[i].id === id){
        this.foodtypes.splice(i, 1)
        return this.getById(id)
        
      }
    }
  }

   create(foodtype: Foodtype): Observable<Foodtype> {
    foodtype.id = this.foodtypes.length + 1
    this.foodtypes.push(foodtype)
    return this.getById(foodtype.id)
  }
 update(foodtype: Foodtype): Observable<Foodtype> {
    for(var i = 0; i<this.foodtypes.length; i++){
      if(this.foodtypes[i].id === foodtype.id){
        this.foodtypes.splice(i, 1, foodtype)
        return this.getById(foodtype.id)
      }
    }
  }

}
