import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Foodtype } from '../foodtype.model';
import { FoodtypeService } from '../foodtype.service';
import { delay, switchMap, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-foodtype-details',
  templateUrl: './foodtype-details.component.html',
  styleUrls: ['./foodtype-details.component.css']
})
export class FoodtypeDetailsComponent implements OnInit, OnDestroy {
  foodtype$: Observable<Foodtype>;

  constructor(
    private foodtypeService: FoodtypeService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.foodtype$ = this.route.paramMap.pipe(
      delay(1500), // Simuleert vertraging
      tap((params: ParamMap) => console.log('foodtype.id = ', params.get('id'))),
      switchMap((params: ParamMap) =>
        this.foodtypeService.getById(parseInt(params.get('id'), 10))
      ),
      tap(console.log)
    );
  }

  delete(foodtypeid: number): void{
    console.log("Deleting:", foodtypeid)
    this.foodtypeService.delete(foodtypeid)
    this.router.navigate(['..'], {relativeTo: this.route})
  }

   ngOnDestroy(): void {
    // Cleanup, niet nodig wanneer je geen subscribe gebruikt.
    // this.paramSubscription.unsubscribe();
  }

}
