import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodtypeDetailsComponent } from './foodtype-details.component';

describe('FoodtypeDetailsComponent', () => {
  let component: FoodtypeDetailsComponent;
  let fixture: ComponentFixture<FoodtypeDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FoodtypeDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodtypeDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
