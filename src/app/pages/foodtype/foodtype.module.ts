import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import * as fromComponents from '.';
import { Routes, RouterModule} from '@angular/router';
import { FoodtypeCreateComponent } from './foodtype-create/foodtype-create.component'
import { FormsModule} from '@angular/forms'

const routes: Routes = [
  {path: '', 
  pathMatch: 'full', 
  component: fromComponents.FoodtypeListComponent},
    {
    path: 'details/:id',
    pathMatch: 'full',
    component: fromComponents.FoodtypeDetailsComponent,
  },
  {
    path: 'details/:id/edit',
    pathMatch: 'full',
    component: fromComponents.FoodtypeCreateComponent,
  },
  {
    path: 'create',
    pathMatch: 'full',
    component: fromComponents.FoodtypeCreateComponent,
  },
];


@NgModule({
  declarations: [...fromComponents.components, FoodtypeCreateComponent],
  imports: [
    CommonModule, RouterModule.forChild(routes), FormsModule],
    
  
})
export class FoodtypeModule { }
