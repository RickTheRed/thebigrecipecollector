import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodtypeEditComponent } from './foodtype-edit.component';

describe('FoodtypeEditComponent', () => {
  let component: FoodtypeEditComponent;
  let fixture: ComponentFixture<FoodtypeEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FoodtypeEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodtypeEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
