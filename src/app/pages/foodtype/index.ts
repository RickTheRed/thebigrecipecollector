import {FoodtypeDetailsComponent} from './foodtype-details/foodtype-details.component';
import {FoodtypeEditComponent} from './foodtype-edit/foodtype-edit.component';
import {FoodtypeListComponent} from './foodtype-list/foodtype-list.component';
import {FoodtypeCreateComponent} from './foodtype-create/foodtype-create.component'
export const components: any[] = [
  FoodtypeDetailsComponent,
  FoodtypeEditComponent,
  FoodtypeListComponent,
  FoodtypeCreateComponent,
];


export * from './foodtype-details/foodtype-details.component';
export * from './foodtype-edit/foodtype-edit.component';
export * from './foodtype-list/foodtype-list.component';
export * from './foodtype-create/foodtype-create.component';