import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodtypeListComponent } from './foodtype-list.component';

describe('FoodtypeListComponent', () => {
  let component: FoodtypeListComponent;
  let fixture: ComponentFixture<FoodtypeListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FoodtypeListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodtypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
