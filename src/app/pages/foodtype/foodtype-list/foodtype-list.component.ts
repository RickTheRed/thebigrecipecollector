import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Foodtype} from '../foodtype.model'
import {FoodtypeService} from '../foodtype.service';
@Component({
  selector: 'app-foodtype-list',
  templateUrl: './foodtype-list.component.html',
  styleUrls: ['./foodtype-list.component.css']
})
export class FoodtypeListComponent implements OnInit {
foodtypes$: Observable<Foodtype[]>
  constructor(private foodtypeService: FoodtypeService) { }

  ngOnInit(): void {
    console.log('List loaded');
    this.foodtypes$ = this.foodtypeService.getAll();
  }

}
