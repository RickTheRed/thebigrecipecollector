import { Component, OnDestroy, OnInit } from '@angular/core';
import { Foodtype } from '../foodtype.model';
import { FoodtypeService } from '../foodtype.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap, tap } from 'rxjs/operators';
import { of, Subscription } from 'rxjs';

@Component({
  selector: 'app-foodtype-create',
  templateUrl: './foodtype-create.component.html',
  styleUrls: ['./foodtype-create.component.css']
})
export class FoodtypeCreateComponent implements OnInit {
  foodtype: Foodtype;
  subscription: Subscription;
  id: number;

  constructor(
    private foodtypeService: FoodtypeService,
    private route: ActivatedRoute,
    private router: Router) 
  
  { }
  

  ngOnInit(): void {
    this.subscription = this.route.paramMap
      .pipe(
        tap(console.log),
        switchMap((params: ParamMap) => {
          if (!params.get('id')) {
            return of({
             name: '',
             alternate_name: '',
             country_of_origin: '',
             allergies: '',
             trivia: '',

            })
          } else {
            return this.foodtypeService.getById(parseInt((params.get('id'))))
          }
        }),
        tap(console.log)
      )
      .subscribe((foodtype) => {
        this.foodtype = foodtype;
        this.id = foodtype.id !== 0 ? foodtype.id : 'Neww foodtype';
      })
  }

  onSubmit(): void {
    console.log('onSubmit', this.foodtype);

    if(this.foodtype.id) {
      console.log('update foodtype');
      this.foodtypeService
        .update(this.foodtype)
        .subscribe((data) =>
          this.router.navigate(['../details', data.id], {relativeTo: this.route }));
    } else {
      console.log('create movie');
      this.foodtypeService
        .create(this.foodtype)
        .subscribe((data) =>
          this.router.navigate(['../details/', data.id], {relativeTo: this.route }))
    }
  }

    ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
