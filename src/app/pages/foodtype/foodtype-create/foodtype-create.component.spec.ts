import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodtypeCreateComponent } from './foodtype-create.component';

describe('FoodtypeCreateComponent', () => {
  let component: FoodtypeCreateComponent;
  let fixture: ComponentFixture<FoodtypeCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FoodtypeCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodtypeCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
