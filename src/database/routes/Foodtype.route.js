const express = require('express')
const router = express.Router()

const foodtype = require('../models/foodtype.model')()
const CrudController = require('../controllers/genericCrud')
//const FoodtypeController = require('../controllers/Foodtype.Controller')
const FoodtypeCrudController = new CrudController(foodtype)



router.post('/', FoodtypeCrudController.create)


router.get('/', FoodtypeCrudController.getAll)


router.get('/:id', FoodtypeCrudController.getOne)


router.put('/:id', FoodtypeCrudController.update)


router.delete('/:id', FoodtypeCrudController.delete)


module.exports = router
