const mongoose = require('mongoose')
//const autoIncrement = require('mongoose-auto-increment');
const Schema = mongoose.Schema

const getmodel = require('./model_cache')

const FoodtypeSchema = new Schema({

name: {
    type: String,
    required: [true, 'A name is required.'],
    
},
alternate_name: String,
country_of_origin: {
type: String,
required: [true, 'A country of origin is required.'],

},
Allergies: String,
Trivia: String,


})

//FoodtypeSchema.plugin(autoIncrement.plugin, 'ID')

module.exports = getmodel('Foodtype', FoodtypeSchema)